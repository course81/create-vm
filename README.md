# Create VM

![alt text](images/1-create_new_vm.png)

![alt text](images/2-custom-vm.png)

![alt text](images/3-vm-compatibility.png)

![alt text](images/4-install-os.png)

![alt text](images/5-choose-os.png) 

![alt text](images/6-vmware-vm-name.png) 

![alt text](images/7-processor.png) 

![alt text](images/8-memory.png) 

![alt text](images/9-vm-network.png) 

![alt text](images/10-io-controller.png) 

![alt text](images/11-disk-type.png) 

![alt text](images/12-create-new-disk.png) 

![alt text](images/13-disk-space.png) 

![alt text](images/14-disk-name.png) 

![alt text](images/15-last-stage-vm.png)

# Install Debian

![alt text](images/16-start-vm.png) 

![alt text](images/17-install-debian.png) 

![alt text](images/18-language.png) 

![alt text](images/19-zone-geo.png) 

![alt text](images/20-keyboard-layout.png) 

> Si vous avez une VM avec plusieurs interfaces réseaux, il faudra sélectionner celle configuré en NAT
>

![alt text](images/21-choose-main-network-interface.png)

![alt text](images/22-vm-name.png) 

![alt text](images/23-domain.png) 

![alt text](images/24-root-password1.png) 

![alt text](images/25-root-password2.png) 

![alt text](images/26-user-login.png) 

![alt text](images/27-user-id.png) 

![alt text](images/28-user-password.png) 

![alt text](images/29-user-password-confirmation.png) 

![alt text](images/30-partitionning-method.png) 

![alt text](images/31-select-disk.png) 

![alt text](images/32-disk-single-partition.png) 

![alt text](images/33-disk-partitionning.png) 

![alt text](images/34-apply-disks-changes.png) 

![alt text](images/35-analyse-paquet.png) 

![alt text](images/36-mirror.png) 

![alt text](images/37-miroir.png) 

![alt text](images/38-proxy.png) 

![alt text](images/39-participate-packet-analysis.png) 

> Attention !!!
> Nous installons un serveur, donc sans interface graphique, pour cela nous allons décocher les 2 première lignes (Environnement de bureau Debian, GNOME) avec la __barre espace__
>

![alt text](images/40-package-install-1.png) 

![alt text](images/40-package-install-2.png) 

> Attention !!! 
> Lors de l'installation de GRUB il faut ABSOLUMMENT sélectionner "/dev/sda" sinon la VM ne démarrera jamais.
>

![alt text](images/41-install-grub.png) 

![alt text](images/42-install-grub-disk.png)